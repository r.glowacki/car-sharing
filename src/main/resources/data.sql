insert into cars(model, car_segment, number_of_seats) values ('Mercedes', 'A', 4);
insert into cars(model, car_segment, number_of_seats) values ('BMW', 'B', 4);
insert into cars(model, car_segment, number_of_seats) values ('Volvo', 'C', 5);
insert into cars(model, car_segment, number_of_seats) values ('Mazda', 'D', 5);
