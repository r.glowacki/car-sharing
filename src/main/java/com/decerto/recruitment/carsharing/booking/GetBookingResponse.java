package com.decerto.recruitment.carsharing.booking;

import com.decerto.recruitment.carsharing.car.GetCarResponse;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.function.Function;

/**
 * Model class for booking REST.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
@Builder
public class GetBookingResponse {

    /**
     * Id.
     */
    private Long id;

    /**
     * Date.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    /**
     * Price.
     */
    private Integer price;

    /**
     * Number of passengers.
     */
    private int numberOfPassengers;

    /**
     * Start city.
     */
    private String startCity;

    /**
     * End city.
     */
    private String endCity;

    /**
     * Booked car.
     */
    private GetCarResponse car;

    /**
     * @return mapper creating data transfer object for {@link Booking}
     */
    public static Function<Booking, GetBookingResponse> entityToDtoMapper() {
        return entity -> GetBookingResponse.builder()
                .id(entity.getId())
                .date(entity.getDate())
                .numberOfPassengers(entity.getNumberOfPassengers())
                .price(entity.getPrice())
                .startCity(entity.getStartCity())
                .endCity(entity.getEndCity())
                .car(GetCarResponse.entityToDtoMapper().apply(entity.getCar()))
                .build();
    }

}
