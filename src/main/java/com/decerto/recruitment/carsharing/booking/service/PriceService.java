package com.decerto.recruitment.carsharing.booking.service;

import com.decerto.recruitment.carsharing.booking.Booking;
import com.decerto.recruitment.carsharing.routes.Routes;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

/**
 * Service responsible for checking price.
 */
@Service
public class PriceService {

    /**
     * @param booking booking
     * @return true whether booking price is higher or equal to those which is set on specified route.
     */
    public boolean checkPrice(Booking booking) {
        return Stream.of(Routes.values())
                .filter(v -> v.getCities().contains(booking.getStartCity()) || v.getCities().contains(booking.getEndCity()))
                .anyMatch(v -> v.getPrice() <= booking.getPrice());
    }

}
