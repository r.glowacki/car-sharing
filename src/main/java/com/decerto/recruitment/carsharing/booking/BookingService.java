package com.decerto.recruitment.carsharing.booking;

import com.decerto.recruitment.carsharing.booking.exception.FoundsNotFoundException;
import com.decerto.recruitment.carsharing.booking.service.PriceService;
import com.decerto.recruitment.carsharing.booking.service.RouteService;
import com.decerto.recruitment.carsharing.booking.service.SeatAvailabilityService;
import com.decerto.recruitment.carsharing.booking.exception.RouteNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * Service responsible for managing bookings.
 */
@Service
@RequiredArgsConstructor
public class BookingService {

    /**
     * Booking repository.
     */
    private final BookingRepository bookingRepository;

    /**
     * Service for managing booking route.
     */
    private final RouteService routeService;

    /**
     * Service for managing booking price.
     */
    private final PriceService priceService;

    /**
     * Service for managing car seats availability.
     */
    private final SeatAvailabilityService seatAvailabilityService;

    /**
     * @return list of selected {@link Booking}
     */
    public Page<Booking> fetchBookings(PageRequest pageRequest) {
        return bookingRepository.findAll(pageRequest);
    }

    /**
     * Method checks if car is available to be booked.
     *
     * @param booking booking
     * @return {@link Booking}
     */
    public Booking addBooking(Booking booking) {

        if (!routeService.checkRoute(booking)) {
            throw new RouteNotFoundException();
        }
        if (!priceService.checkPrice(booking)) {
            throw new FoundsNotFoundException();
        }
        seatAvailabilityService.checkAvailableSeats(booking);

        return bookingRepository.save(booking);
    }

}
