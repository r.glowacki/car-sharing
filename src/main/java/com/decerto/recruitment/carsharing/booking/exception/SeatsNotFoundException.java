package com.decerto.recruitment.carsharing.booking.exception;

/**
 * Class responsible for handling exception.
 */
public class SeatsNotFoundException extends RuntimeException {

    /**
     * Message send when SeatsNotFoundException occur.
     */
    public SeatsNotFoundException() {
        super("There are no available seats for selected number of passengers.");
    }

}
