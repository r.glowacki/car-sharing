package com.decerto.recruitment.carsharing.booking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Exception handler which overrides {@link HttpStatus} code.
 */
@ControllerAdvice
public class BookingExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * @param response response
     * @throws IOException in case of failed or interrupted I/O operations
     */
    @ExceptionHandler(RouteNotFoundException.class)
    public void handleRouteNotFound(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    /**
     * @param response response
     * @throws IOException in case of failed or interrupted I/O operations
     */
    @ExceptionHandler(FoundsNotFoundException.class)
    public void handleFoundsNotFound(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    /**
     * @param response response
     * @throws IOException in case of failed or interrupted I/O operations
     */
    @ExceptionHandler(SeatsNotFoundException.class)
    public void handleSeatsNotFound(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

}
