package com.decerto.recruitment.carsharing.booking;

import com.decerto.recruitment.carsharing.car.Car;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.function.BiFunction;

/**
 * Class representing user request.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
@Builder
public class PostUserRequest {

    /**
     * Booking date.
     */
    @NotNull(message = "Please provide date")
    private LocalDate date;

    /**
     * Price
     */
    @NotNull(message = "Please provide price")
    private Integer price;

    /**
     * Number of passengers.
     */
    @NotNull(message = "Please provide number of passengers")
    private int numberOfPassengers;

    /**
     * Start city.
     */
    @NotNull(message = "Please provide start city")
    private String startCity;

    /**
     * End city.
     */
    @NotNull(message = "Please provide end city")
    private String endCity;

    /**
     * @return mapper creating data transfer object for {@link PostUserRequest} and {@link Car}
     */
    public static BiFunction<PostUserRequest, Car, Booking> requestToEntityMapper() {
        return (userRequest, selectedCar) -> Booking.builder()
                .date(userRequest.date)
                .numberOfPassengers(userRequest.numberOfPassengers)
                .price(userRequest.price)
                .startCity(userRequest.startCity)
                .endCity(userRequest.endCity)
                .car(selectedCar)
                .build();
    }

}
