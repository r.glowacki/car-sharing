package com.decerto.recruitment.carsharing.booking.exception;

/**
 * Class responsible for handling exception.
 */
public class FoundsNotFoundException extends RuntimeException {

    /**
     * Message send when FoundsNotFoundException occur.
     */
    public FoundsNotFoundException() {
        super("You do not have sufficient funds");
    }

}
