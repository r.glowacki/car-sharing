package com.decerto.recruitment.carsharing.booking.service;

import com.decerto.recruitment.carsharing.booking.Booking;
import com.decerto.recruitment.carsharing.booking.BookingRepository;
import com.decerto.recruitment.carsharing.booking.exception.SeatsNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service responsible for seat availability.
 */
@Service
@RequiredArgsConstructor
public class SeatAvailabilityService {

    /**
     * Booking repository.
     */
    private final BookingRepository bookingRepository;

    /**
     * Method check whether there are enough available seats for selected number of passengers.
     *
     * @param booking booking
     */
    public void checkAvailableSeats(Booking booking) {
        final Long carId = booking.getCar().getId();
        final LocalDate bookingDate = booking.getDate();
        final String startCity = booking.getStartCity();
        final String endCity = booking.getEndCity();

        final Optional<Integer> optionalPassengers = bookingRepository
                .findTotalNumberOfPassengersByCarIdAndDateAndStartCityOrEndCity(carId, bookingDate, startCity, endCity);

        if (optionalPassengers.isPresent()) {
            int totalNumberOfPassengers = optionalPassengers.get();
            final int passengers = totalNumberOfPassengers + booking.getNumberOfPassengers();
            if (passengers > booking.getCar().getNumberOfSeats()) {
                throw new SeatsNotFoundException();
            }
        }
    }

}
