package com.decerto.recruitment.carsharing.booking;

import com.decerto.recruitment.carsharing.car.Car;
import com.decerto.recruitment.carsharing.car.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller responsible managing bookings.
 */
@RestController
@RequestMapping("/booking")
@RequiredArgsConstructor
public class BookingController {

    /**
     * Service for managing bookings.
     */
    private final BookingService bookingService;

    /**
     * Service for managing cars.
     */
    private final CarService carService;

    /**
     * Mapping for getting of selected bookings.
     *
     * @return list of {@link GetBookingResponse}
     */
    @GetMapping
    public List<GetBookingResponse> fetchBookings(@RequestParam(required = false, defaultValue = "0") int offset,
                                                  @RequestParam(required = false, defaultValue = "50") int limit) {
        return bookingService.fetchBookings(PageRequest.of(offset, limit))
                .stream()
                .map(value -> GetBookingResponse.entityToDtoMapper()
                        .apply(value))
                .collect(Collectors.toList());
    }

    /**
     * Mapping for adding new booking.
     *
     * @param userRequest user request
     * @param carSegment  car segment
     * @return {@link GetBookingResponse}
     */
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public GetBookingResponse addBooking(@Valid @RequestBody PostUserRequest userRequest, @RequestParam String carSegment) {
        final Optional<Car> selectedCar = carService.getCarByCarSegment(carSegment);
        if (selectedCar.isPresent()) {
            return GetBookingResponse.entityToDtoMapper()
                    .apply(bookingService.addBooking(PostUserRequest.requestToEntityMapper().apply(userRequest, selectedCar.get())));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Car in selected segment not found");
        }
    }

}
