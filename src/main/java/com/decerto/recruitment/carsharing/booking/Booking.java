package com.decerto.recruitment.carsharing.booking;

import com.decerto.recruitment.carsharing.car.Car;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.LocalDate;

/**
 * Entity class for booking.
 */
@Entity
@Table(name = "bookings")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode
@ToString
public class Booking {

    /**
     * Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Booking date.
     */
    private LocalDate date;

    /**
     * Price
     */
    private Integer price;

    /**
     * Number of passengers.
     */
    @Column(name = "number_of_passengers")
    private int numberOfPassengers;

    /**
     * Start city.
     */
    @Column(name = "start_city")
    private String startCity;

    /**
     * End city.
     */
    @Column(name = "end_city")
    private String endCity;

    /**
     * Booked car.
     */
    @ManyToOne
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    @JsonIgnoreProperties("bookings")
    private Car car;

}
