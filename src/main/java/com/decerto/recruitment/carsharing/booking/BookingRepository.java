package com.decerto.recruitment.carsharing.booking;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Optional;

public interface BookingRepository extends PagingAndSortingRepository<Booking, Long> {

    @Query("SELECT SUM(b.numberOfPassengers) FROM Booking b where b.car.id = :carId and b.date = :date and (b.startCity = :startCity or b.endCity = :endCity)")
    Optional<Integer> findTotalNumberOfPassengersByCarIdAndDateAndStartCityOrEndCity(@Param("carId") Long id, @Param("date") LocalDate date, @Param("startCity") String startCity, @Param("endCity") String endCity);

}

