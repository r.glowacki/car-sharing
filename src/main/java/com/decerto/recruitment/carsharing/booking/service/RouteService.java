package com.decerto.recruitment.carsharing.booking.service;

import com.decerto.recruitment.carsharing.booking.Booking;
import com.decerto.recruitment.carsharing.routes.Routes;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

/**
 * Service responsible for checking route.
 */
@Component
public class RouteService {

    /**
     * @param booking booking
     * @return true whether selected start city or selected end city is in {@link Routes} as a places where you can go to or go from.
     */
    public boolean checkRoute(Booking booking) {
        return Stream.of(Routes.values())
                .anyMatch(v -> v.getCities().contains(booking.getStartCity()) || v.getCities().contains(booking.getEndCity()));
    }

}
