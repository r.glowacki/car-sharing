package com.decerto.recruitment.carsharing.booking.exception;

/**
 * Class responsible for handling exception.
 */
public class RouteNotFoundException extends RuntimeException {

    /**
     * Message send when RouteNotFoundException occur.
     */
    public RouteNotFoundException() {
        super("Car not found for selected route");
    }

}
