package com.decerto.recruitment.carsharing.car;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarRepository extends PagingAndSortingRepository<Car, Long> {

    Optional<Car> findCarByCarSegment(String carSegment);

}
