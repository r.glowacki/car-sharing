package com.decerto.recruitment.carsharing.car;

import com.decerto.recruitment.carsharing.booking.Booking;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Entity class for car.
 */
@Entity
@Table(name = "cars")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode
@ToString
public class Car {

    /**
     * Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Model.
     */
    private String model;

    /**
     * Segment.
     */
    private String carSegment;

    /**
     * Number of seats in car.
     */
    @Column(name = "number_of_seats")
    private int numberOfSeats;

    /**
     * Bookings.
     */
    @OneToMany(mappedBy = "car")
    @JsonIgnoreProperties("car")
    private List<Booking> bookings;

}
