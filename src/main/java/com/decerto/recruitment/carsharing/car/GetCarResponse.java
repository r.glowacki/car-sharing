package com.decerto.recruitment.carsharing.car;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.function.Function;

/**
 * Model class for car REST.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
@ToString
@Builder
public class GetCarResponse {

    /**
     * Id.
     */
    private Long id;

    /**
     * Model.
     */
    private String model;

    /**
     * Car segment.
     */
    private String carSegment;

    /**
     * Number of seats.
     */
    private int numberOfSeats;

    /**
     * @return mapper creating data transfer object for {@link Car}
     */
    public static Function<Car, GetCarResponse> entityToDtoMapper() {
        return entity -> GetCarResponse.builder()
                .id(entity.getId())
                .model(entity.getModel())
                .carSegment(entity.getCarSegment())
                .numberOfSeats(entity.getNumberOfSeats())
                .build();
    }

}
