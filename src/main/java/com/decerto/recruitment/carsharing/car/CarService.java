package com.decerto.recruitment.carsharing.car;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service responsible for managing cars.
 */
@Service
@RequiredArgsConstructor
public class CarService {

    /**
     * Car repository.
     */
    private final CarRepository carRepository;

    /**
     * @param carSegment car segment
     * @return {@link Car} if selected car segment exist
     */
    public Optional<Car> getCarByCarSegment(String carSegment) {
        return carRepository.findCarByCarSegment(carSegment);
    }

}
