package com.decerto.recruitment.carsharing.routes;

import lombok.Getter;

import java.util.List;

/**
 * Contains available routes with list of cities and price.
 */
public enum Routes {

    /**
     * Route WAW_KRK.
     */
    WAW_KRK(List.of("Warszawa", "Kraków"), 100),

    /**
     * Route GDA_WAW.
     */
    GDA_WAW(List.of("Gdańsk", "Warszawa"), 74),

    /**
     * Route GDA_WRO.
     */
    GDA_WRO(List.of("Gdańsk", "Warszawa", "Łódź", "Wrocław"), 177),

    /**
     * Route POZ_KAT.
     */
    POZ_KAT(List.of("Poznań", "Wrocław", "Katowice"), 155);

    /**
     * List of cities.
     */
    @Getter
    private final List<String> cities;

    /**
     * Price.
     */
    @Getter
    private final Integer price;

    /**
     * @param cities list of cities
     * @param price  price
     */
    Routes(List<String> cities, Integer price) {
        this.cities = cities;
        this.price = price;
    }

}
