package com.decerto.recruitment.carsharing.booking;

import com.decerto.recruitment.carsharing.car.Car;
import com.decerto.recruitment.carsharing.car.CarRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class BookingRepositoryTest {

    @Autowired
    public BookingRepository bookingRepository;

    @Autowired
    public CarRepository carRepository;

    @Test
    public void findTotalNumberOfPassengersByCarIdAndDate_oneBookings_correctResponse() {
        Car car = Car.builder()
                .id(1L)
                .carSegment("A")
                .model("foo")
                .numberOfSeats(4)
                .build();
        Booking booking = Booking.builder()
                .car(car)
                .id(1L)
                .date(LocalDate.of(2020, 12, 12))
                .numberOfPassengers(2)
                .price(100)
                .startCity("Warszawa")
                .endCity("Kraków")
                .build();

        carRepository.save(car);
        bookingRepository.save(booking);

        final Integer passengers = bookingRepository
                .findTotalNumberOfPassengersByCarIdAndDateAndStartCityOrEndCity(car.getId(), booking.getDate(), booking.getStartCity(), booking.getEndCity()).get();
        assertThat(passengers).isEqualTo(2);
    }

    @Test
    public void findTotalNumberOfPassengersByCarIdAndDate_severalBookings_correctResponse() {
        Car car = Car.builder()
                .id(1L)
                .carSegment("A")
                .model("foo")
                .numberOfSeats(4)
                .build();
        List<Booking> bookings = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            bookings.add(Booking.builder()
                    .car(car)
                    .id((long) i)
                    .date(LocalDate.of(2020, 10, 7))
                    .numberOfPassengers(1)
                    .price(100)
                    .startCity("Warszawa")
                    .endCity("Kraków")
                    .build());
        }

        carRepository.save(car);
        bookingRepository.saveAll(bookings);

        final Integer passengers = bookingRepository
                .findTotalNumberOfPassengersByCarIdAndDateAndStartCityOrEndCity(car.getId(), bookings.get(0).getDate(), bookings.get(0).getStartCity(), bookings.get(0).getEndCity()).get();
        assertThat(passengers).isEqualTo(4);
    }

    @Test
    public void findTotalNumberOfPassengersByCarIdAndDate_severalBookingsInDiffDays_correctResponse() {
        Car car = Car.builder()
                .id(1L)
                .carSegment("A")
                .model("foo")
                .numberOfSeats(4)
                .build();
        List<Booking> bookings = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            bookings.add(Booking.builder()
                    .car(car)
                    .id((long) i)
                    .date(LocalDate.of(2020, 10, 7).plusDays(i))
                    .numberOfPassengers(1)
                    .price(100)
                    .startCity("Warszawa")
                    .endCity("Kraków")
                    .build());
        }

        carRepository.save(car);
        bookingRepository.saveAll(bookings);

        final Integer passengers = bookingRepository
                .findTotalNumberOfPassengersByCarIdAndDateAndStartCityOrEndCity(car.getId(), bookings.get(0).getDate(), bookings.get(0).getStartCity(), bookings.get(0).getEndCity()).get();
        assertThat(passengers).isEqualTo(1);

        final Integer passengersNext = bookingRepository
                .findTotalNumberOfPassengersByCarIdAndDateAndStartCityOrEndCity(car.getId(), bookings.get(1).getDate(), bookings.get(1).getStartCity(), bookings.get(1).getEndCity()).get();
        assertThat(passengersNext).isEqualTo(1);
    }

}
