package com.decerto.recruitment.carsharing.car;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class CarRepositoryTest {

    @Autowired
    public CarRepository carRepository;

    @Test
    public void findCarByCarSegment_correctCarSegment_correctResponse() {
        final Optional<Car> car = carRepository.findCarByCarSegment("A");

        assertThat(car.get().getId()).isEqualTo(1L);
    }

    @Test
    public void findCarByCarSegment_incorrectCarSegment_correctResponse() {
        final Optional<Car> car = carRepository.findCarByCarSegment("F");

        assertThat(car).isEqualTo(Optional.empty());
    }

}
