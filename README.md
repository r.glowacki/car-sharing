# CarSharing

This is application used to book car.

## Description

//TODO 

### Requirements

Requirements for building and running:

* Java Development Kit 11.x.x,
* Apache Maven 3.5.x.

### Building

In order to build use:

```bash
mvn package
```

### Running

In order to run use:

```bash
java -jar target/carsharing-0.0.1-SNAPSHOT.jar
```

There are four car's segments available: A, B, C and D (capitalization does not matter) with described number of seats.

In order to add new booking use `POST` request on:
```
localhost:8080/car-rental/booking?carSegment="A"
```

Body request have to be added including: date, price, number of passengers, start city and end city.

There are several routes with start and end city (sometimes also with city on the route), with price established which user has to pay. 

If both start and end city is incorrect booking is not possible.
If price is too low booking is not possible.
Moreover, if all seats have already been booked, new booking is not possible.

Example: 
```json
{"date": "2020-10-10", "price": 190, "numberOfPassengers": 2, "startCity": "Warszawa", "endCity": "Kraków"}
```

curl

```
curl -v -X POST localhost:8080/car-rental/booking 
        -H "Content-type:application/json" 
        -d "{\"date\":\"2020-10-10\",\"price\":190,\"numberOfPassengers\":2,\"startCity\":\"Warszawa\",\"endCity\":\"Kraków\"}"
```

In order to check all bookings use `GET` request:

```
localhost:8080/booking
```

curl

```
curl -v loclahost:8080/car-rental/booking?offset=<offset>&limit=<limit>
```


Tematem zadania jest aplikacja pośrednicząca w wynajmie aut z naciskiem na car-sharing.

Podstawową funkcjonalnością aplikacji jest przyjmowanie od klientów zleceń zawierających:

klasę auta,
kierunek podróży rozumiany jako zbiór miast,
termin tj. zakres dat rozumiany jako termin początku podróży,
maksymalną dopuszczalną opłatę.
Każde z miast posiada konkretną cenę, która jest bardzo zróżnicowana, ponieważ odwzorowuje koszt paliwa i czas najmu auta. 
Gdy maksymalna dopuszczalna opłata określona przez klienta jest wyższa lub równa kwocie wynikającej z kierunku podróży, zlecenie jest zatwierdzane, 
a w przeciwnym przypadku - wstrzymywane. W czasie przychodzenia kolejnych podobnych zleceń sprawdzane jest, 
czy nie ma takich dwóch, trzech lub czterech zleceń, które mogą zostać połączone, gdy:

klasa auta jest identyczna,
ich termin się pokrywa (co najmniej jeden dzień),
kierunek jest zbieżny - zlecenia tworzą łańcuch, tj. można je połączyć po co najmniej jednym wspólnym mieście.
Należy pamiętać, iż w momencie łączenia zleceń zbiór miast rośnie, więc i koszt zlecenia. 
Klient, którego zlecenie nie zawierało konkretnego miasta, musi również pokryć jego koszt.

Zarówno klasy aut jak i miasta są wartościami ze stałego niezmiennego zbioru (np. segment A, B, C; Warszawa, Kraków, Wrocław).

W zadaniu można świadomie zignorować aspekty wydajnościowe związane z kombinatoryką (łączenie zleceń), 
jak również problemy wyznaczania najkrótszej trasy (kolejność miast na trasie nie ma znaczenia).

Aplikacja udostępnia API do zakładania zleceń oraz sprawdzania ich stanu.
